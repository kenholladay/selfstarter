class PreorderController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => :ipn

  def index
  end

  def checkout
  end

  def prefill
    @user  = User.find_or_create_by_email!(params[:email])

    price = params[:amount]

    @order = Order.prefill!(:name => Settings.product_name, :price => price, :user_id => @user.id)

    # This is where all the magic happens. We create a multi-use token with Amazon, letting us charge the user's Amazon account
    # Then, if they confirm the payment, Amazon POSTs us their shipping details and phone number
    # From there, we save it, and voila, we got ourselves a preorder!

    Stripe.api_key = STRIPE_PRIVATE_KEY
    charge_response = Stripe::Charge.create(
        :amount => (@order.price * 100).to_i,
        :currency => "usd",
        :card => params[:stripeToken],
        :description => Settings.product_name
      )
    @order.token = params[:stripeToken]
    @order.save
  rescue Stripe::CardError => e
    # Since it's a decline, Stripe::CardError will be caught
    body = e.json_body
    err  = body[:error]
    flash[:card_error] = err
  rescue Stripe::InvalidRequestError => e
    # Invalid parameters were supplied to Stripe's API
  rescue Stripe::AuthenticationError => e
    # Authentication with Stripe's API failed
    # (maybe you changed API keys recently)
  rescue Stripe::APIConnectionError => e
    # Network communication with Stripe failed
  rescue Stripe::StripeError => e
    # Display a very generic error to the user, and maybe send
    # yourself an email
  rescue => e
    throw e
  ensure
    redirect_to action: :complete
  end

  def complete
    @error = flash[:card_error] if flash[:card_error]
  end

  def share
    @order = Order.find_by_uuid(params[:uuid])
  end

  def ipn
  end
end
