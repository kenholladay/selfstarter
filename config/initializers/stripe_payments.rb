require "stripe"
if Rails.env.production?
	Stripe.api_key = "#{Settings.stripe_production_secret_api_key}"
	STRIPE_PRIVATE_KEY =  "#{Settings.stripe_production_secret_api_key}"	
	STRIPE_PUBLIC_KEY = "#{Settings.stripe_production_public_api_key}"
else
	Rails.logger.info "Using TEST API Keys for STRIPE"
	Stripe.api_key = "#{Settings.stripe_development_secret_api_key}"
	STRIPE_PRIVATE_KEY =  "#{Settings.stripe_development_secret_api_key}"	
	STRIPE_PUBLIC_KEY =  "#{Settings.stripe_development_public_api_key}"
end
